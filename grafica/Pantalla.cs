﻿using OpenTK.Windowing.Common;
using OpenTK.Windowing.Desktop;
using OpenTK.Windowing.GraphicsLibraryFramework;
using System;
using System.Collections.Generic;
using System.Text;
using OpenTK.Input;
using OpenTK.Graphics.OpenGL4;
using System.Linq;
using OpenTK.Mathematics;
using System.Drawing;
using System.Drawing.Imaging;

namespace grafica
{
    class Pantalla : GameWindow
    {
        private double time;
        bool primero = true;
        bool uno = true;
        bool dos = false;
        bool TXU = false;
        bool TYU = false;
        bool TZU = false;
        bool RXU = false;
        bool RYU = false;
        bool RZU = false;
        bool SXU = false;
        bool SYU = false;
        bool SZU = false;
        bool TXD = false;
        bool TYD = false;
        bool TZD = false;
        bool RXD = false;
        bool RYD = false;
        bool RZD = false;
        bool SXD = false;
        bool SYD = false;
        bool SZD = false;
        bool change = false;
        Dibujo actualD;
        Parte actualP;
        public Escenario escena = new Escenario();
        Dibujo mesa = new Dibujo();       
        Dibujo silla = new Dibujo();        
        public Pantalla(GameWindowSettings config, NativeWindowSettings nativo) : base(config, nativo) { }
        protected override void OnLoad()
        {
            base.OnLoad();
            GL.Enable(EnableCap.DepthTest);

            mesa.mesa(new Vector3(-0.5f, 0.4f, -0.5f), 1f, 1f, 0.6f, new Vector3(0.0f, -0.5f, -2f), new Vector3(0, 0, 0), new Vector3(1, 1, 1), new Vector2(0.0f, 0.1f), new Vector2(0.8f, 1.0f));
            silla.silla(new Vector3(-0.15f, 0.15f, -0.7f), 0.4f, 0.4f, 0.85f, new Vector3(0.0f, -0.5f, -2f), new Vector3(0, 0, 0), new Vector3(1, 1, 1), new Vector2(0.0f, 0.1f), new Vector2(0.6f, 0.8f));
            escena.addDibujo("mesa",mesa);
            escena.addDibujo("silla",silla);
            actualD = escena.dibujos[escena.Dactual()];
            actualP = escena.dibujos[escena.Dactual()].partes[actualD.Pactual()];
        }
        protected override void OnUnload()
        {
            base.OnUnload();
            escena.Unload();
        }
        protected override void OnRenderFrame(FrameEventArgs e)
        {
            base.OnRenderFrame(e);
            time += e.Time;

            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            GL.ClearColor(0.5f, 0.0f, 0.5f, 1.0f);
            GL.Viewport(0, 0, Size.X, Size.Y);
            
            if (!uno) {
                if (dos)
                {
                    if (change && time > 2.5f || primero && change)
                    {
                        if (primero)
                        {
                            primero = false;
                        }
                        escena.changeActual();
                        actualD = escena.dibujos[escena.Dactual()];
                        time = 0;
                    }
                    if (TYU)
                    {
                        escena.changeDibujo(escena.Dactual(), new Vector3(0, 0.01f, 0), null, null);
                        TYU = false;
                    }
                    if (TYD)
                    {
                        escena.changeDibujo(escena.Dactual(), new Vector3(0, -0.01f, 0), null, null);
                        TYD = false;
                    }
                    if (TXU)
                    {
                        escena.changeDibujo(escena.Dactual(), new Vector3(0.01f, 0, 0), null, null);
                        TXU = false;
                    }
                    if (TXD)
                    {
                        escena.changeDibujo(escena.Dactual(), new Vector3(-0.01f, 0, 0), null, null);
                        TXD = false;
                    }

                    if (TZU)
                    {
                        escena.changeDibujo(escena.Dactual(), new Vector3(0, 0, 0.01f), null, null);
                        TZU = false;
                    }
                    if (TZD)
                    {
                        escena.changeDibujo(escena.Dactual(), new Vector3(0, 0, -0.01f), null, null);
                        TZD = false;
                    }
                    if (RYD)
                    {
                        escena.changeDibujo(escena.Dactual(), null, new Vector3(0, -0.01f, 0), null);
                        RYD = false;
                    }
                    if (RYU)
                    {
                        escena.changeDibujo(escena.Dactual(), null, new Vector3(0, 0.01f, 0), null);
                        RYU = false;
                    }
                    if (RXD)
                    {
                        escena.changeDibujo(escena.Dactual(), null, new Vector3(-0.01f, 0, 0), null);
                        RXD = false;
                    }
                    if (RXU)
                    {
                        escena.changeDibujo(escena.Dactual(), null, new Vector3(0.01f, 0, 0), null);
                        RXU = false;
                    }
                    if (RZD)
                    {
                        escena.changeDibujo(escena.Dactual(), null, new Vector3(0, 0, -0.01f), null);
                        RZD = false;
                    }
                    if (RZU)
                    {
                        escena.changeDibujo(escena.Dactual(), null, new Vector3(0, 0, 0.01f), null);
                        RZU = false;
                    }
                    if (SYD)
                    {
                        escena.changeDibujo(escena.Dactual(), null, null, new Vector3(0, -0.01f, 0));
                        SYD = false;
                    }
                    if (SYU)
                    {
                        escena.changeDibujo(escena.Dactual(), null, null, new Vector3(0, 0.01f, 0));
                        SYU = false;
                    }
                    if (SXD)
                    {
                        escena.changeDibujo(escena.Dactual(), null, null, new Vector3(-0.01f, 0, 0));
                        SXD = false;
                    }
                    if (SXU)
                    {
                        escena.changeDibujo(escena.Dactual(), null, null, new Vector3(0.01f, 0, 0));
                        SXU = false;
                    }
                    if (SZD)
                    {
                        escena.changeDibujo(escena.Dactual(), null, null, new Vector3(0, 0, -0.01f));
                        SZD = false;
                    }
                    if (SZU)
                    {
                        escena.changeDibujo(escena.Dactual(), null, null, new Vector3(0, 0, 0.01f));
                        SZU = false;
                    }
                }
                else
                {
                    if (change && time > 2.5f || primero && change)
                    {
                        if (primero)
                        {
                            primero = false;
                        }
                        escena.dibujos[escena.Dactual()].changeActual();
                        actualP = escena.dibujos[escena.Dactual()].partes[escena.dibujos[escena.Dactual()].Pactual()];
                        time = 0;
                    }
                    if (TYU)
                    {
                        actualD.changeItem(actualD.Pactual(), new Vector3(0, 0.01f, 0), null, null);
                        TYU = false;
                    }
                    if (TYD)
                    {
                        actualD.changeItem(actualD.Pactual(), new Vector3(0, -0.01f, 0), null, null);
                        TYD = false;
                    }
                    if (TXU)
                    {
                        actualD.changeItem(actualD.Pactual(), new Vector3(0.01f, 0, 0), null, null);
                        TXU = false;
                    }
                    if (TXD)
                    {
                        actualD.changeItem(actualD.Pactual(), new Vector3(-0.01f, 0, 0), null, null);
                        TXD = false;
                    }

                    if (TZU)
                    {
                        actualD.changeItem(actualD.Pactual(), new Vector3(0, 0, 0.01f), null, null);
                        TZU = false;
                    }
                    if (TZD)
                    {
                        actualD.changeItem(actualD.Pactual(), new Vector3(0, 0, -0.01f), null, null);
                        TZD = false;
                    }
                    if (RYD)
                    {
                        actualD.changeItem(actualD.Pactual(), null, new Vector3(0, -0.01f, 0), null);
                        RYD = false;
                    }
                    if (RYU)
                    {
                        actualD.changeItem(actualD.Pactual(), null, new Vector3(0, 0.01f, 0), null);
                        RYU = false;
                    }
                    if (RXD)
                    {
                        actualD.changeItem(actualD.Pactual(), null, new Vector3(-0.01f, 0, 0), null);
                        RXD = false;
                    }
                    if (RXU)
                    {
                        actualD.changeItem(actualD.Pactual(), null, new Vector3(0.01f, 0, 0), null);
                        RXU = false;
                    }
                    if (RZD)
                    {
                        actualD.changeItem(actualD.Pactual(), null, new Vector3(0, 0, -0.01f), null);
                        RZD = false;
                    }
                    if (RZU)
                    {
                        actualD.changeItem(actualD.Pactual(), null, new Vector3(0, 0, 0.01f), null);
                        RZU = false;
                    }
                    if (SYD)
                    {
                        actualD.changeItem(actualD.Pactual(), null, null, new Vector3(0, -0.01f, 0));
                        SYD = false;
                    }
                    if (SYU)
                    {
                        actualD.changeItem(actualD.Pactual(), null, null, new Vector3(0, 0.01f, 0));
                        SYU = false;
                    }
                    if (SXD)
                    {
                        actualD.changeItem(actualD.Pactual(), null, null, new Vector3(-0.01f, 0, 0));
                        SXD = false;
                    }
                    if (SXU)
                    {
                        actualD.changeItem(actualD.Pactual(), null, null, new Vector3(0.01f, 0, 0));
                        SXU = false;
                    }
                    if (SZD)
                    {
                        actualD.changeItem(actualD.Pactual(), null, null, new Vector3(0, 0, -0.01f));
                        SZD = false;
                    }
                    if (SZU)
                    {
                        actualD.changeItem(actualD.Pactual(), null, null, new Vector3(0, 0, 0.01f));
                        SZU = false;
                    }
                }
            }
            else
            {
                if (TYU)
                {
                    escena.changeall(new Vector3(0, 0.01f, 0), null, null);
                    TYU = false;
                }
                if (TYD)
                {
                    escena.changeall(new Vector3(0, -0.01f, 0), null, null);
                    TYD = false;
                }
                if (TXU)
                {
                    escena.changeall(new Vector3(0.01f, 0, 0), null, null);
                    TXU = false;
                }
                if (TXD)
                {
                    escena.changeall(new Vector3(-0.01f, 0, 0), null, null);
                    TXD = false;
                }

                if (TZU)
                {
                    escena.changeall(new Vector3(0, 0, 0.01f), null, null);
                    TZU = false;
                }
                if (TZD)
                {
                    escena.changeall(new Vector3(0, 0, -0.01f), null, null);
                    TZD = false;
                }
                if (RYD)
                {
                    escena.changeall(null, new Vector3(0, -0.01f, 0), null);
                    RYD = false;
                }
                if (RYU)
                {
                    escena.changeall(null, new Vector3(0, 0.01f, 0), null);
                    RYU = false;
                }
                if (RXD)
                {
                    escena.changeall(null, new Vector3(-0.01f, 0, 0), null);
                    RXD = false;
                }
                if (RXU)
                {
                    escena.changeall(null, new Vector3(0.01f, 0, 0), null);
                    RXU = false;
                }
                if (RZD)
                {
                    escena.changeall(null, new Vector3(0, 0, -0.01f), null);
                    RZD = false;
                }
                if (RZU)
                {
                    escena.changeall(null, new Vector3(0, 0, 0.01f), null);
                    RZU = false;
                }
                if (SYD)
                {
                    escena.changeall(null, null, new Vector3(0, -0.01f, 0));
                    SYD = false;
                }
                if (SYU)
                {
                    escena.changeall(null, null, new Vector3(0, 0.01f, 0));
                    SYU = false;
                }
                if (SXD)
                {
                    escena.changeall(null, null, new Vector3(-0.01f, 0, 0));
                    SXD = false;
                }
                if (SXU)
                {
                    escena.changeall(null, null, new Vector3(0.01f, 0, 0));
                    SXU = false;
                }
                if (SZD)
                {
                    escena.changeall(null, null, new Vector3(0, 0, -0.01f));
                    SZD = false;
                }
                if (SZU)
                {
                    escena.changeall(null, null, new Vector3(0, 0, 0.01f));
                    SZU = false;
                }
            }
            change = false;
            escena.render(Size);
            Context.SwapBuffers();
        }
        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            KeyboardState input = KeyboardState;

            if (input.IsKeyDown(Keys.D1))
            {
                uno = true;
                dos = false;
            }

            if (input.IsKeyDown(Keys.D2))
            {
                uno = false;
                dos = true;
            }

            if (input.IsKeyDown(Keys.D3))
            {
                uno = false;
                dos = false;
            }

            if (input.IsKeyDown(Keys.Escape))
            {
                this.Close();
            }

            if (input.IsKeyDown(Keys.Tab))
            {
                change = true;
            }

            if (input.IsKeyDown(Keys.X))
            {
                TYD = true;
            }

            if (input.IsKeyDown(Keys.Space))
            {
                TYU = true;
            }

            if (input.IsKeyDown(Keys.Up))
            {
                TZD = true;
            }

            if (input.IsKeyDown(Keys.Down))
            {
                TZU = true;
            }

            if (input.IsKeyDown(Keys.Left))
            {
                TXD = true;
            }

            if (input.IsKeyDown(Keys.Right))
            {
                TXU = true;
            }

            if (input.IsKeyDown(Keys.W))
            {
                RYD = true;
            }

            if (input.IsKeyDown(Keys.S))
            {
                RYU = true;
            }

            if (input.IsKeyDown(Keys.A))
            {
                RXD = true;
            }

            if (input.IsKeyDown(Keys.D))
            {
                RXU = true;
            }
            if (input.IsKeyDown(Keys.Q))
            {
                RZD = true;
            }

            if (input.IsKeyDown(Keys.E))
            {
                RZU = true;
            }

            if (input.IsKeyDown(Keys.U))
            {
                SYD = true;
            }

            if (input.IsKeyDown(Keys.J))
            {
                SYU = true;
            }

            if (input.IsKeyDown(Keys.H))
            {
                SXD = true;
            }

            if (input.IsKeyDown(Keys.K))
            {
                SXU = true;
            }

            if (input.IsKeyDown(Keys.Y))
            {
                SZD = true;
            }

            if (input.IsKeyDown(Keys.I))
            {
                SZU = true;
            }
            base.OnUpdateFrame(e);
        }
    }
}
