﻿using OpenTK.Mathematics;
using OpenTK.Windowing.Desktop;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace grafica
{
    public partial class Form1 : Form
    {
        Pantalla game;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            GameWindowSettings config = new GameWindowSettings();
            config.RenderFrequency = 30.0;
            config.UpdateFrequency = 60.0;
            NativeWindowSettings nativo = new NativeWindowSettings();
            nativo.Size = new Vector2i(800, 600);
            nativo.Title = "Programacion Grafica";
            using (game = new Pantalla(config, nativo))
            {
                game.Run();
            }

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (XT.Text == "")
                XT.Text = "0";
            if (YT.Text == "")
                YT.Text = "0";
            if (ZT.Text == "")
                ZT.Text = "0";
            if (XR.Text == "")
                XR.Text = "0";
            if (YR.Text == "")
                YR.Text = "0";
            if (ZR.Text == "")
                ZR.Text = "0";
            if (XS.Text == "")
                XS.Text = "0";
            if (YS.Text == "")
                YS.Text = "0";
            if (ZS.Text == "")
                ZS.Text = "0";
            bool parte = true;
            if (comboBox1.SelectedItem.ToString() == "escenario")
            {
                game.escena.changeall(new Vector3(float.Parse(XT.Text), float.Parse(YT.Text), float.Parse(ZT.Text)), new Vector3(float.Parse(XR.Text), float.Parse(YR.Text), float.Parse(ZR.Text)), new Vector3(float.Parse(XS.Text), float.Parse(YS.Text), float.Parse(ZS.Text)));
            }
            else
            {
                Dibujo aux = game.escena.dibujos.GetValueOrDefault(comboBox1.SelectedItem.ToString());
                if(aux != null)
                {
                    game.escena.changeDibujo(comboBox1.SelectedItem.ToString(), new Vector3(float.Parse(XT.Text), float.Parse(YT.Text), float.Parse(ZT.Text)), new Vector3(float.Parse(XR.Text), float.Parse(YR.Text), float.Parse(ZR.Text)), new Vector3(float.Parse(XS.Text), float.Parse(YS.Text), float.Parse(ZS.Text)));
                }
                else
                {
                    string[] select = comboBox1.SelectedItem.ToString().Split(" ");
                    game.escena.changeParte(select[0], select[1], new Vector3(float.Parse(XT.Text), float.Parse(YT.Text), float.Parse(ZT.Text)), new Vector3(float.Parse(XR.Text), float.Parse(YR.Text), float.Parse(ZR.Text)), new Vector3(float.Parse(XS.Text), float.Parse(YS.Text), float.Parse(ZS.Text)));
                }
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            comboBox1.Items.Add("escenario");
            foreach (KeyValuePair<string, Dibujo> act in game.escena.dibujos)
            {
                comboBox1.Items.Add(act.Key);
                foreach (KeyValuePair<string, Parte> p in act.Value.partes)
                {
                    comboBox1.Items.Add(act.Key+" "+p.Key);
                }
            }
            comboBox1.SelectedIndex = 0;
        }
    }
}
