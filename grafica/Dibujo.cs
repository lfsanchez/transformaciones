﻿using OpenTK.Graphics.OpenGL;
using OpenTK.Mathematics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace grafica
{
    class Dibujo
    {
        public Dictionary<string, Parte> partes;
        private int actual = 1;

        public Dibujo()
        {
            partes = new Dictionary<string, Parte>();
        }

        public void Load()
        {
            foreach (KeyValuePair<string, Parte> act in partes)
            {
                act.Value.Load();
            }
        }

        public void unload()
        {
            foreach (KeyValuePair<string, Parte> act in partes)
            {
                act.Value.unload();
            }
        }

        public void render(float width, float height)
        {
            foreach (KeyValuePair<string, Parte> act in partes)
            {
                act.Value.render(width,height);
            }
        }

        public void addParte(String nombre, Vector3 posicion, float Ancho, float Largo, float Alto, Vector3 t, Vector3 angle, Vector3 s, Vector2 X, Vector2 Y)
        {
            Parte act = new Parte(posicion, Ancho, Largo, Alto, t, angle, s, X, Y);
            partes.Add(nombre, act);
        }

        public void changeItem(String key, Object tras, Object rot, Object sca)
        {
            Parte aux = partes.GetValueOrDefault(key);
            if (tras != null)
            {
                Vector3 t = (Vector3)tras;
                aux.tras = new Vector3(aux.tras.X + t.X, aux.tras.Y + t.Y, aux.tras.Z + t.Z);
            }
            if (rot != null)
            {
                Vector3 r = (Vector3)rot;
                aux.angulo = new Vector3(aux.angulo.X + r.X, aux.angulo.Y + r.Y, aux.angulo.Z + r.Z);
            }
            if (sca != null)
            {
                Vector3 s = (Vector3)sca;
                aux.sca = new Vector3(aux.sca.X + s.X, aux.sca.Y + s.Y, aux.sca.Z + s.Z);
            }
        }

        public void changeall(Object tras, Object rot, Object sca)
        {
            foreach (KeyValuePair<string, Parte> aux in partes)
            {
                if (tras != null)
                {
                    Vector3 t = (Vector3)tras;
                    aux.Value.tras = new Vector3(aux.Value.tras.X + t.X, aux.Value.tras.Y + t.Y, aux.Value.tras.Z + t.Z);
                    aux.Value.pos = new Vector3(aux.Value.pos.X + t.X, aux.Value.pos.Y + t.Y, aux.Value.pos.Z + t.Z);
                }
                if (rot != null)
                { 
                    Vector3 r = (Vector3)rot;
                    aux.Value.angulo = new Vector3(aux.Value.angulo.X + r.X, aux.Value.angulo.Y + r.Y, aux.Value.angulo.Z + r.Z);
                }
                if (sca != null)
                {
                    Vector3 s = (Vector3)sca;
                    aux.Value.sca = new Vector3(aux.Value.sca.X + s.X, aux.Value.sca.Y + s.Y, aux.Value.sca.Z + s.Z);
                }
            }
        }

        public void mesa(Vector3 posicion, float Ancho, float Largo, float Alto, Vector3 t, Vector3 angle, Vector3 s, Vector2 X, Vector2 Y)
        {
            float lar;
            if (Ancho > Largo)
            {
                lar = Ancho;
            }
            else
            {
                lar = Largo;
            }
            Parte basemesa = new Parte(posicion, Ancho, Largo, Alto*10/100, t, angle, s, X, Y);
            Parte pata1 = new Parte(new Vector3(posicion.X+Largo*10/100,posicion.Y- Alto * 10 / 100, posicion.Z-Ancho*10/100), lar * 10 / 100, lar * 10 / 100, Alto-Alto * 10 / 100, t, angle, s, X, Y);
            Parte pata2 = new Parte(new Vector3(posicion.X + Largo * 10 / 100, posicion.Y - Alto * 10 / 100, posicion.Z - Ancho + Ancho * 10 / 100 +lar*10/100), lar * 10 / 100, lar * 10 / 100, Alto - Alto * 10 / 100, t, angle, s, X, Y);
            Parte pata3 = new Parte(new Vector3(posicion.X + Largo - Largo * 10 / 100 - lar * 10 / 100, posicion.Y - Alto * 10 / 100, posicion.Z - Ancho * 10 / 100 - lar * 10 / 100), lar * 10 / 100, lar * 10 / 100, Alto - Alto * 10 / 100, t, angle, s, X, Y);
            Parte pata4 = new Parte(new Vector3(posicion.X + Largo - Largo * 10 / 100 - lar * 10 / 100, posicion.Y - Alto * 10 / 100, posicion.Z - Ancho + Ancho * 10 / 100 + lar * 10 / 100), lar * 10 / 100, lar * 10 / 100, Alto - Alto * 10 / 100, t, angle, s, X, Y);
            partes.Add("base", basemesa);
            partes.Add("pata1", pata1);
            partes.Add("pata2", pata2);
            partes.Add("pata3", pata3);
            partes.Add("pata4", pata4);
        }

        public void silla(Vector3 posicion, float Ancho, float Largo, float Alto, Vector3 t, Vector3 angle, Vector3 s, Vector2 X, Vector2 Y)
        {
            float lar;
            if (Ancho > Largo)
            {
                lar = Ancho;
            }
            else
            {
                lar = Largo;
            }
            Parte basesilla = new Parte(posicion, Ancho, Largo, Alto * 5 / 100, t, angle, s, X, Y);
            Parte espaldar = new Parte(new Vector3(posicion.X,posicion.Y, posicion.Z-Ancho+ Ancho * 10 / 100), Ancho * 10 / 100, Largo , -Alto * 50 / 100, t, angle, s, X, Y);
            Parte pata1 = new Parte(new Vector3(posicion.X + Largo * 10 / 100, posicion.Y - Alto * 5 / 100, posicion.Z - Ancho * 10 / 100), lar * 15 / 100, lar * 15 / 100, Alto * 45/100 - Alto * 5 / 100, t, angle, s, X, Y);
            Parte pata2 = new Parte(new Vector3(posicion.X + Largo * 10 / 100, posicion.Y - Alto * 5 / 100, posicion.Z - Ancho + Ancho * 10 / 100 +lar*15/100), lar * 15 / 100, lar * 15 / 100, Alto * 45 / 100 - Alto * 5 / 100, t, angle, s, X, Y);
            Parte pata3 = new Parte(new Vector3(posicion.X + Largo - Largo * 10 / 100 -lar*15/100, posicion.Y - Alto * 5 / 100, posicion.Z - Ancho * 10 / 100), lar * 15 / 100, lar * 15 / 100, Alto * 45 / 100 - Alto * 5 / 100, t, angle, s, X, Y);
            Parte pata4 = new Parte(new Vector3(posicion.X + Largo - Largo * 10 / 100 - lar * 15/ 100, posicion.Y - Alto * 5 / 100, posicion.Z - Ancho + Ancho * 10 / 100 +lar*15/100), lar * 15 / 100, lar * 15 / 100, Alto * 45 / 100 - Alto * 5/ 100, t, angle, s, X, Y);
            partes.Add("base", basesilla);
            partes.Add("espaldar", espaldar);
            partes.Add("pata1", pata1);
            partes.Add("pata2", pata2);
            partes.Add("pata3", pata3);
            partes.Add("pata4", pata4);
        }

        public void changeActual()
        {
            if (this.actual >= partes.Count)
            {
                this.actual = 1;
            }
            else
            {
                this.actual = this.actual + 1;
            }
        }
        public string Pactual()
        {
            int i = 1;
            foreach (KeyValuePair<string, Parte> act in partes)
            {
                if (i == this.actual)
                {
                    return act.Key;
                }
                i++;
            }
            return null;
        }
        /*
        public void robot(Vector2 X, Vector2 Y)
        {
            float lar;
            if (largo > ancho)
            {
                lar = largo;
            }
            else
            {

                lar = ancho;
            }
            float[] cabeza = Parte(pos, alto * 15 / 100, alto * 15 / 100, alto * 15 / 100, X, Y);
            float[] cuello = Parte(new Vector3(pos.X + alto * 15 * 40 / 10000, pos.Y - alto * 15 / 100, pos.Z - alto * 15 * 40 / 10000), alto * 15 / 500, alto * 15 / 500, alto * 5 / 100, X, Y);
            float[] torso = Parte(new Vector3(pos.X - (largo - (alto * 15 / 100)) / 2, pos.Y - alto * 20 / 100, pos.Z), ancho, largo, alto * 40 / 100, X, Y);
            float[] brazo_derecho = Parte(new Vector3(pos.X - (largo - (alto * 15 / 100)) / 2 - ancho * 50 / 100, pos.Y - alto * 20 / 100, pos.Z - ancho * 25/100), ancho * 50 / 100, ancho * 50 / 100, alto * 30 / 100, X, Y);
            float[] brazo_izquierdo = Parte(new Vector3(pos.X + (alto * 15 / 100) + (largo - (alto * 15 / 100)) / 2 , pos.Y - alto * 20 / 100, pos.Z - ancho *25/100), ancho * 50 / 100, ancho * 50 / 100, alto * 30 / 100, X, Y);
            float[] pierna_derecha = Parte(new Vector3(pos.X - (largo - (alto * 15 / 100)) / 2, pos.Y - alto * 60 / 100, pos.Z - ancho * 25 / 100), ancho * 50 / 100, ancho * 50 / 100, alto * 40 / 100, X, Y);
            float[] pierna_izquierda = Parte(new Vector3(pos.X - (largo - (alto * 15 / 100)) / 2 + largo - ancho * 50 / 100, pos.Y - alto * 60 / 100, pos.Z - ancho * 25 / 100), ancho * 50 / 100, ancho * 50 / 100, alto * 40 / 100, X, Y);
            float[] pie_derecho = Parte(new Vector3(pos.X - (largo - (alto * 15 / 100)) / 2, pos.Y - alto * 95 / 100, pos.Z + ancho * 25 / 100), ancho * 50 / 100, ancho * 50 / 100, alto * 5 / 100, X, Y);
            float[] pie_izquierdo = Parte(new Vector3(pos.X - (largo - (alto * 15 / 100)) / 2 + largo - ancho * 50 / 100, pos.Y - alto * 95 / 100, pos.Z + ancho * 25 / 100), ancho * 50 / 100, ancho * 50 / 100, alto * 5 / 100, X, Y);
            vertices = cabeza.Concat(cuello).ToArray().Concat(torso).ToArray().Concat(brazo_derecho).ToArray().Concat(brazo_izquierdo).ToArray().Concat(pierna_derecha).ToArray().Concat(pierna_izquierda).ToArray().Concat(pie_izquierdo).ToArray().Concat(pie_derecho).ToArray();
            Load();
        }*/
    }
}
